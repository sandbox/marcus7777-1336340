<?php

/**
 * Server Admin callbacks and required functions.
 */
function community_cdn_ui_server_overview() {
  $header = array(t('Server Name'), t('Storage'), t('Actions'));
  $rows = array();
  // Always clear the server cache on this display.
  foreach (community_cdn_servers(TRUE) as $server) {
    $row = array();
    $row[] = l($server['servername'], 'admin/build/community_cdn/'. $server['serverid']);
    $links = array();
    switch ($server['storage']) {
      case COMMUNITY_CDN_STORAGE_DEFAULT:
        $row[] = t('Default');
        $links[] = l(t('View'), 'admin/build/community_cdn/'. $server['serverid']);
        $links[] = l(t('Flush'), 'admin/build/community_cdn/'. $server['serverid'] .'/flush' );
        break;
      case COMMUNITY_CDN_STORAGE_OVERRIDE:
        $row[] = t('Override');
        $links[] = l(t('Edit'), 'admin/build/community_cdn/'. $server['serverid']);
        $links[] = l(t('Revert'), 'admin/build/community_cdn/'. $server['serverid'] .'/delete');
        $links[] = l(t('Flush'), 'admin/build/community_cdn/'. $server['serverid'] .'/flush' );
        $links[] = l(t('Export'), 'admin/build/community_cdn/'. $server['serverid'] .'/export' );
        break;
      case COMMUNITY_CDN_STORAGE_NORMAL:
        $row[] = t('Normal');
        $links[] = l(t('Edit'), 'admin/build/community_cdn/'. $server['serverid']);
        $links[] = l(t('Delete'), 'admin/build/community_cdn/'. $server['serverid'] .'/delete');
        $links[] = l(t('Flush'), 'admin/build/community_cdn/'. $server['serverid'] .'/flush' );
        $links[] = l(t('Export'), 'admin/build/community_cdn/'. $server['serverid'] .'/export' );
        break;
    }
    $row[] = implode('&nbsp;&nbsp;&nbsp;&nbsp;', $links);
    $rows[] = $row;
  }
  $output = theme('table', $header, $rows);
  return $output;
}

function community_cdn_ui_server_delete_form($form_state, $server = array()) {
  if (empty($server)) {
    drupal_set_message(t('The specified server was not found'), 'error');
    drupal_goto('admin/build/community_cdn');
  }

  $form = array();
  $form['serverid'] = array('#type' => 'value', '#value' => $server['serverid']);
  return confirm_form(
    $form,
    t('Are you sure you want to delete the server %server?',
      array('%server' => $server['servername'])
    ),
    'admin/build/community_cdn',
    t('This action cannot be undone.'),
    t('Delete'),  t('Cancel')
  );
}

function community_cdn_ui_server_delete_form_submit($form, &$form_state) {
  $server = community_cdn_server($form_state['values']['serverid']);
  community_cdn_server_delete($server);
  drupal_set_message(t('Server %name (ID: @id) was deleted.', array('%name' => $server['servername'], '@id' => $server['serverid'])));
  $form_state['redirect'] = 'admin/build/community_cdn';
}


function community_cdn_ui_server_flush_form(&$form_state, $server = array()) {
  if (empty($server)) {
    drupal_set_message(t('The specified server was not found'), 'error');
    $form_state['redirect'] = 'admin/build/community_cdn';
  }

  $form = array();
  $form['serverid'] = array('#type' => 'value', '#value' => $server['serverid']);
  return confirm_form(
    $form,
    t('Are you sure you want to flush the server %server?',
      array('%server' => $server['servername'])
    ),
    'admin/build/community_cdn',
    t('This action cannot be undone.'),
    t('Flush'),  t('Cancel')
  );
}

function community_cdn_ui_server_flush_form_submit($form, &$form_state) {
  $server = community_cdn_server($form_state['values']['serverid']);
  community_cdn_server_flush($server);
  drupal_set_message(t('Server %name (ID: @id) was flushed.', array('%name' => $server['servername'], '@id' => $server['serverid'])));
  $form_state['redirect'] = 'admin/build/community_cdn';
}


/**
 * Community CDN server export form.
 */
function community_cdn_ui_server_export_form(&$form_state, $server = array()) {
  if (empty($server)) {
    drupal_set_message(t('The specified server was not found'), 'error');
    $form_state['redirect'] = 'admin/build/community_cdn';
  }

  if (isset($server['serverid'])) {
    unset($server['serverid']);
  }
  if (isset($server['storage'])) {
    unset($server['storage']);
  }
  foreach ($server['actions'] as $id => $action) {
    unset($server['actions'][$id]['actionid']);
    unset($server['actions'][$id]['serverid']);
  }
  $exported = '$servers = array();';
  $exported .= "\n";
  $exported .= '$servers[\''. $server['servername'] .'\'] = ';
  $exported .= var_export($server, TRUE) .';';
  $rows = substr_count($exported, "\n") + 1;

  $form = array();
  $form['export'] = array(
    '#type' => 'textarea',
    '#default_value' => $exported,
    '#rows' => $rows,
    '#resizable' => FALSE,
  );
  return $form;
}


function community_cdn_ui_server_form($form_state, $server = array()) {
  $form = array();

  $form['serverid'] = array(
    '#type' => 'value',
    '#value' => isset($server['serverid']) ? $server['serverid'] : '',
  );

  // Browsers don't submit disabled form values so we've got to put two copies
  // of the name on the form: one for the submit handler and one for the user.
  if (isset($server['storage']) && $server['storage'] === COMMUNITY_CDN_STORAGE_DEFAULT) {
    $form['servername'] = array(
      '#type' => 'value',
      '#value' => $server['servername'],
    );
    $form['servername_display'] = array(
      '#type' => 'textfield',
      '#size' => '64',
      '#title' => t('Trusted Server Domain Name'),
      '#default_value' => $server['servername'],
      '#disabled' => TRUE,
    );
  }
  else {
    $form['servername'] = array(
      '#type' => 'textfield',
      '#size' => '64',
      '#title' => t('Trusted Server Domain Name'),
      '#default_value' => isset($server['servername']) ? $server['servername'] : '',
      '#description' => t('This is used in URL\'s for file to tell Community CDN how to process an file. Please only use alphanumeric characters, underscores (_), hyphens (-) and points (.) for domain names.'),
      '#validate' => array('community_cdn_element_servername_validate' => array()),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($server['storage']) && $server['storage'] === COMMUNITY_CDN_STORAGE_DEFAULT ? t('Override Defaults') : t('Save Server'),
    '#weight' => 9,
  );

// TODO: add form stuff

  $community_cdn_path = community_cdn_create_url($server['servername'], $preview_path, TRUE);

  return $form;
}

function community_cdn_ui_server_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  // Check for duplicates
  foreach (community_cdn_servers() as $server) {
    if ($values['servername'] == $server['servername'] && $values['serverid'] != $server['serverid']) {
      form_set_error('servername', t('The server name you have chosen is already in use.'));
      break;
    }
  }

  // Check for illegal characters in server names
  if (preg_match('/[^0-9a-zA-Z._\-]/', $values['servername'])) {
    form_set_error('servername', t('Please only use alphanumeric characters, underscores (_), and hyphens (-) for server names.'));
  }
}

function community_cdn_ui_server_form_submit($form, &$form_state) {
  // Save the server first to retrieve the serverid when overriding
  $server = community_cdn_server_save($form_state['values']);

  // Populate the serverid as needed for overrides
  if (isset($form_state['values']['actions'])) {
    foreach($form_state['values']['actions'] as $action) {
      if (empty($action['serverid'])) {
        $action['serverid'] = $server['serverid'];
      }
      community_cdn_action_save($action);
    }
  }

  // Go back to the server form
  $form_state['redirect'] = 'admin/build/community_cdn/'. $server['serverid'];
}